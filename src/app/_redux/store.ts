import {TODO} from './todo'

//Our Actions that will trigger a State change in our App, through the Store:
import {ADD_TODO, TOOGLE_TODO, REMOVE_TODO, REMOVE_ALL_TODOS} from './actions';



export interface AppState{
    todos: TODO[];
    lastUpDate: Date;
}
export const INIT_STATE: AppState = {todos: [], lastUpDate: null}


//Our Actions => (new) State code:
export function rootReducer(state: AppState, action): AppState {
    console.log('Redux Store: Old state is', state);

    //Do the coding for each Action, that will trigger a State change (a new Obj Store State) in our App:
    switch(action.type){
        
        /**
         * =====================================================================================
         */
        case ADD_TODO:
            // We have a new TODO:
            //action.todo.id = state.todos.length + 1;
            const
                uniqueId = Math.random().toString(36).substring(2)  + (new Date()).getTime().toString(36)

            ;
            action.todo.id = uniqueId;

            // Set the new state Obj:
            return(
                Object.assign({}, state, {
                    todos       : state.todos.concat(Object.assign({}, action.todo))
                    , lastUpDate: new Date()
                })
            );
        
        /**
         * =====================================================================================
         */
        case TOOGLE_TODO:
            console.error('Redux Store: Received action:', action);

            // Set the new state Obj, changing "isComplete" property of TODO with id=action.id:
            const
                thisTodo       = state.todos.find(eachTodo => eachTodo.id === action.id)
                , thisTodo_idx = state.todos.indexOf(thisTodo);
                
            ;
            return(
                Object.assign({}, state, {
                    todos       : [
                        ...state.todos.slice(0, thisTodo_idx),
                        Object.assign({}, thisTodo, {isComplete: !thisTodo.isComplete}),
                        ...state.todos.slice(thisTodo_idx+1)
                    ]
                    , lastUpDate: new Date()
                })
            );
        
        /**
         * =====================================================================================
         */
        case REMOVE_TODO:
            //Remove todo who's id=action.id:
            return(
                Object.assign({}, state, {
                    todos       : state.todos.filter(eachTodo => eachTodo.id !== action.id)
                    , lastUpDate: new Date()
                })
            );

        /**
         * =====================================================================================
         */
        case REMOVE_ALL_TODOS:
            return state;

    }

    // Strange... We need to return SOMETHING!
    // rootReducer() wil be called in instantiation, and no "action" has been taken...
    return state;
}

