export interface TODO{
    id         : number,
    description: string,
    responsible: string,
    priority   : string,
    isComplete : boolean

}