import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
/** Angular FontAwesome library */
import { AngularFontAwesomeModule } from 'angular-font-awesome';

/* The Angular Redux dependencies: */
import { NgRedux, NgReduxModule } from "@angular-redux/store";
/* Our Redux Store: */
import {AppState, INIT_STATE, rootReducer} from './_redux/store';


/* Our Components: */
import { AppComponent } from './app.component';
import { TodoHeaderComponent } from "./views/todo-header/todo-header.component";
import { TodoInsertComponent } from "./views/todo-insert/todo-insert.component";
import { TodoListComponent } from "./views/todo-list/todo-list.component";


@NgModule({
  declarations: [
    AppComponent,
    TodoHeaderComponent,
    TodoInsertComponent,
    TodoListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgReduxModule,            //Import the Redux library, so we can inject it to do our Store, managing the whole App State.
    AngularFontAwesomeModule  // Import Angular FontAwesome library - don't forget to add "../node_modules/font-awesome/css/font-awesome.css", into .angular-cli.json, since we're using it
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {

  //Inject the Redux library into our entire App:
  constructor(ngRedux: NgRedux<AppState>) {
    ngRedux.configureStore(rootReducer, INIT_STATE)
  }

}
