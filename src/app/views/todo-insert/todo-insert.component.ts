import { Component, OnInit } from '@angular/core';

/* The Angular Redux dependencies: */
import { NgRedux, select } from "@angular-redux/store";

/* Our Redux Store: */
import { AppState } from '../../_redux/store';
//Our Actions that will trigger a State change in our App, through the Store:
import { ADD_TODO } from '../../_redux/actions';
/* Our TODO item structure: */
import { TODO } from '../../_redux/todo';



@Component({
	selector: 'app-todo-insert',
	templateUrl: './todo-insert.component.html',
	styleUrls: ['./todo-insert.component.css']
})

export class TodoInsertComponent implements OnInit {

	// The @select decorator is used to gain access to the todos properties from the store
	@select() todos;

	// Our data Model:
	model: TODO = {
		id: 0,
		description: "",
		responsible: "",
		priority: "low",
		isComplete: false
	};
	constructor(
		private ngRedux: NgRedux<AppState>
	)
	{ }

	ngOnInit() {
	}

	onSubmit(addTodoForm) {
		if(addTodoForm.invalid)
			return;
	 
		console.error('TodoInsertComponent: "this" Component (should have the form and data!)', addTodoForm, this);
		// this.ngRedux.dispatch({type: ADD_TODO}) <= you can add properties to the dispatch action...
		this.ngRedux.dispatch({type: ADD_TODO, todo: this.model});

		//Reset form, but assuem a defaul value for the "priority" selector:
		addTodoForm.reset();
		addTodoForm.controls.priority.setValue("low");

	}

}
