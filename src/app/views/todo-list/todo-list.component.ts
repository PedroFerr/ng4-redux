import { Component, OnInit } from '@angular/core';
/** Angular FontAwesome library */
import { AngularFontAwesomeModule } from 'angular-font-awesome';

/* The Angular Redux dependencies: */
import { NgRedux, select } from "@angular-redux/store";

/* Our Redux Store: */
import { AppState } from '../../_redux/store';
//Our Actions that will trigger a State change in our App, through the Store:
import { TOOGLE_TODO, REMOVE_TODO } from '../../_redux/actions';
/* Our TODO item structure: */
import { TODO } from '../../_redux/todo';



@Component({
	selector: 'app-todo-list',
	templateUrl: './todo-list.component.html',
	styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

	// The @select decorator is used to gain access to the todos properties from the store
	@select() todos;

	// Our "toto" data Model:
	todo: TODO = {
		id: 0,
		description: "",
		responsible: "",
		priority: "low",
		isComplete: false
	};

	constructor(
		private ngRedux: NgRedux<AppState>
	)
	{ }

	ngOnInit() {

	}

	// Our view events handlers to our State Store:
	// 1st - toggle is/is not completed:
	toggleTodo(thisTodo){
		console.warn('TodoListComponent: someone is TOGGLEing ', thisTodo, '"isComplete" state to ', !thisTodo.isComplete);

		//Dispatch it to our store:
		this.ngRedux.dispatch(
			{
				type        : TOOGLE_TODO
				, id        : thisTodo.id
				, isComplete: !thisTodo.isComplete
			}
		);
	}
	// 2nd - delete THIS (id) TODO:
	deleteTodo(thisTodo){
		console.warn('TodoListComponent: someone wants to DELETE ', thisTodo);
		
		window.confirm('The traditional "Are you sure....?"') ?
			this.ngRedux.dispatch({type: REMOVE_TODO, id: thisTodo.id})
			:
			null
		;
		
	}

}
