import { Component, OnInit } from '@angular/core';


/* The Angular Redux dependencies: */
import { NgRedux, select } from "@angular-redux/store";

/* Our Redux Store: */
import { AppState } from '../../_redux/store';


@Component({
	selector: 'app-todo-header',
	templateUrl: './todo-header.component.html',
	styleUrls: ['./todo-header.component.css']
})

export class TodoHeaderComponent implements OnInit {

	// The @select decorator is used to gain access to the todos properties from the store
	@select() todos;
	//Get the last App State update, from interface AppState:
	@select() lastUpDate;


	constructor(
		private ngRedux: NgRedux<AppState>
	)
	{ }

	ngOnInit() {
	}

}
